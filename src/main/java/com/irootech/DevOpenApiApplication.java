package com.irootech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevOpenApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevOpenApiApplication.class, args);
	}

}
