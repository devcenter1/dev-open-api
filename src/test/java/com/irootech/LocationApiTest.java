package com.irootech;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest
public class LocationApiTest {

    /**
     * 查询设备轨迹
     *
     * 失败
     */
    @Test
    public void queryDeviceTrack() {
        ArrayList<Map<String, Object>> requestBody = new ArrayList<>();
        Map<String, Object> body = new HashMap<>();
        body.put("deviceUuid", "52NFJ9836K07");
        body.put("startDateTime", lastDate());
        body.put("endDateTime", nowDate());

        body.put("intervalValue", 0);
        body.put("page", "1");
        body.put("size", "50");
        requestBody.add(body);
        String result = RequestUtil.postRequestForArray("/dev-api/api/v1/queryDeviceTrack", requestBody);
        System.out.println("queryDeviceTrack result：" + result);
    }

    /**
     * 设置设备定点接口
     *
     * 成功
     */
    @Test
    public void setDeviceLocation() {
        HashMap<String, Object> requestBody = new HashMap<>();

        List<Map<String, Object>> deviceList = new ArrayList<>();
        Map<String, Object> body = new HashMap<>();
        body.put("deviceUuid", "52NFJ9836K07");
        body.put("latitude", "36.09954");
        body.put("longitude", "38.71164");

        deviceList.add(body);
        requestBody.put("deviceList", deviceList);
        String result = RequestUtil.postRequest("/dev-api/api/v1/setDeviceLocation", requestBody);
        System.out.println("setDeviceLocation result：" + result);
    }

    private static String lastDate() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, -1);
        return formatDate(c.getTime());
    }

    private static String nowDate() {
        return formatDate(new Date());
    }

    private static String formatDate(Date date) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.CHINA);
        return sdf.format(date);
    }
}
