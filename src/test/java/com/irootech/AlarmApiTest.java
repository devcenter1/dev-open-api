package com.irootech;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class AlarmApiTest {

    /**
     * 查询报警数据
     *
     * 成功
     */
    @Test
    public void queryAlarmDataTest() {
        HashMap<String, Object> requestBody = new HashMap<>();
        List<String> deviceUuids = new ArrayList<>();
        deviceUuids.add("51ORYQMZ9RK7");
        requestBody.put("deviceUuid", deviceUuids);
//        requestBody.put("alarmSeverity", "");
//        requestBody.put("alarmStatus", "");
//        requestBody.put("alarmTopicclass", "");
//        requestBody.put("startAlarmDatetime", LocalDateTime.now());
//        requestBody.put("endAlarmDatetime", LocalDateTime.now());
        requestBody.put("page", 1);
        requestBody.put("size", 10);

        String result = RequestUtil.postRequest("/dev-api/api/v1/queryAlarmData", requestBody);
        System.out.println("queryAlarmData result：" + result);
    }
}
