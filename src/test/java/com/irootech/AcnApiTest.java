package com.irootech;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
public class AcnApiTest {

    @Test
    public void queryDeviceListTest() {
        HashMap<String, String> param = new HashMap<>();
//        param.put("tenantId", "47SEB09");
        param.put("tenantId", "8LQ6V01");
        param.put("page", "1");
        param.put("size", "10");
        String result = RequestUtil.getRequestForParams("/acn/proxy/sys/device/list", param);
        System.out.println("queryDeviceListTest result：" + result);
    }

    @Test
    public void queryDeviceStatisticsTest() {
        HashMap<String, String> param = new HashMap<>();
        param.put("groupType", "day");
        param.put("deviceId", "4TEWEKTHFOW7");
        String result = RequestUtil.getRequestForParams("/acn/proxy/sys/device/statistics", param);
        System.out.println("queryDeviceStatisticsTest result：" + result);
    }

    @Test
    public void getDeviceInfoTest() {
        HashMap<String, Object> requestBody = new HashMap<>();
        requestBody.put("deviceId", "4TEWEKTHFOW7");
        String result = RequestUtil.postRequest("/acn/proxy/sys/device/Info", requestBody);
        System.out.println("getDeviceInfoTest result：" + result);
    }
}
