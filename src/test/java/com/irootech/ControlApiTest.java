package com.irootech;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
public class ControlApiTest {

    /**
     * 指令下发
     */
    @Test
    public void deviceControl() {
        HashMap<String, Object> requestBody = new HashMap<>();
        requestBody.put("control_option", "0"); // TODO showdoc 沒有此字段
        requestBody.put("deviceUuid", "52NFJ9836K07");
        requestBody.put("uuid", Calendar.getInstance().getTimeInMillis());
        List<Map<String, String>> kvList = new ArrayList<>();
        Map<String, String> kvMap = new HashMap<>();
        kvMap.put("measurementNameEn", "tag1");
        kvMap.put("value", "2");
        kvList.add(kvMap);
        requestBody.put("kv", kvList);
        String result = RequestUtil.postRequest("/dev-api/api/control/v1/deviceControl", requestBody);
        System.out.println("deviceControl result：" + result);
    }

    /**
     * 数据刷新
     */
    @Test
    public void refreshData() {
        HashMap<String, Object> requestBody = new HashMap<>();
        requestBody.put("deviceUuid", "52NFJ9836K07");
        String result = RequestUtil.postRequest("/dev-api/api/control/v1/refreshData", requestBody);
        System.out.println("refreshData result：" + result);
    }

}
