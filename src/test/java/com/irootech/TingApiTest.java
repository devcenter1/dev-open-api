package com.irootech;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
public class TingApiTest {

    /**
     * 获取物模型
     * 无数据
     */
    @Test
    public void getThingModelTest() {
        HashMap<String, Object> requestBody = new HashMap<>();
        requestBody.put("modelId", "T310RYFRZ5PS8");
        String result = RequestUtil.postRequest("/dev-api/api/v1/getThingModel", requestBody);
        System.out.println("deviceControl result：" + result);
    }

    /**
     * 获取租户下模型信息列表
     * 无数据
     */
    @Test
    public void getThingModelByTenantTest() {
        HashMap<String, Object> requestBody = new HashMap<>();
        String result = RequestUtil.postRequest("/dev-api/api/v1/getThingModelByTenant", requestBody);
        System.out.println("getThingModelByTenant result：" + result);
    }

    /**
     * 获取租户下模型信息列表
     * <p>
     * 成功
     */
    @Test
    public void thingsOpenTest() {
        HashMap<String, String> param = new HashMap<>();
        param.put("tenantId_equals", "49OLU08");
//        param.put("modelName_contains", ""); // 非必填
        param.put("page", "1");
        param.put("size", "10");
        String result = RequestUtil.getRequestForParams("/boss/boss-thing/api/V1/boss-thing/things-open", param);
        System.out.println("things-open result：" + result);
    }

    /**
     * 获取租户下物模型的报警设置信息
     * 成功
     */
    @Test
    public void alarmsOpenTest() {
        HashMap<String, String> param = new HashMap<>();
        param.put("modelId", "T310RYFRZ5PS8");
        param.put("tenantId", "49OLU08");
//        param.put("alarmTopicname", ""); // 非必填
//        param.put("alarmTopicclass", ""); // 非必填
        param.put("page", "1");
        param.put("size", "10");
        String result = RequestUtil.getRequestForParams("/boss/boss-thing/api/V1/boss-thing/things/alarms-open", param);
        System.out.println("alarms-open result：" + result);
    }
}
