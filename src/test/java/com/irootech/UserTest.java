package com.irootech;

import com.rootcloud.spruce.api.sdk.client.SignClient;
import com.rootcloud.spruce.api.sdk.client.support.DefaultSignClient;
import com.rootcloud.spruce.api.sdk.common.base.CommonRequest;
import com.rootcloud.spruce.api.sdk.common.enums.ContentType;
import com.rootcloud.spruce.api.sdk.common.enums.HttpMethod;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 卫华专属云生产环境
 */
@SpringBootTest
public class UserTest {

    /**
     * 获取信息验证码(手机号+验证码 登录)
     *
     */
    @Test
    public void getDeviceHistoryData() {
        Map<String,String> param=new HashMap<>();
        param.put("telphone","18800000014");
        String result = RequestUtil.getRequestForParams("/base/base-identity/api/user/verification-code-for-login",param);
        System.out.println("getDeviceHistoryData result：" + result);
    }

    public void gettest01(){
        HashMap<String,Object> param=new HashMap<>();
        param.put("serviceType","saas-gba-icsm");
        param.put("tenantName","卫华测试555");
        param.put("effectiveTime",1);
        param.put("openingTime","2021-07-30");
        param.put("tenantId","B59UX07");
        param.put("appCode","FSM003");

        String result=RequestUtil.postRequest("/saas/saas-aba-base/api/manager/distribute-app",param);
        System.out.println(result);
    }


    private static String lastDate() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, -1);
        return formatDate(c.getTime());
    }

    private static String nowDate() {
        return formatDate(new Date());
    }

    private static String formatDate(Date date) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.CHINA);
        return sdf.format(date);
    }
}
