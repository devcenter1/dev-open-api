package com.irootech;

import com.rootcloud.spruce.api.sdk.client.SignClient;
import com.rootcloud.spruce.api.sdk.client.support.DefaultSignClient;
import com.rootcloud.spruce.api.sdk.common.base.CommonRequest;
import com.rootcloud.spruce.api.sdk.common.enums.ContentType;
import com.rootcloud.spruce.api.sdk.common.enums.HttpMethod;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest
public class DeviceApiTest {

    /**
     * 获取设备历史数据
     * 无数据
     */
    @Test
    public void getDeviceHistoryData() {
        ArrayList<Map<String, Object>> requestBody = new ArrayList<>();
        Map<String, Object> body = new HashMap<>();
        body.put("deviceUuid", "51ORYQMZ9RK7");
        body.put("properties", new String[]{"Timestamp_Local", "time_cloud", "variable"});
//        body.put("fillType", 0);
//        body.put("insertType", 0);
        body.put("startDateTime", "2021-07-01T13:33:36.000Z");
        body.put("endDateTime", "2021-07-20T23:33:36.000Z");
        body.put("page", "1");
        body.put("size", "10");
        requestBody.add(body);
        String result = RequestUtil.postRequestForArray("/dev-api/api/v1/getDeviceHistoryData", requestBody);
        System.out.println("getDeviceHistoryData result：" + result);
    }

    /**
     * 查询设备的详细信息
     *
     * 成功
     */
    @Test
    public void queryDeviceDetail() {
        HashMap<String, Object> requestBody = new HashMap<>();
        List<String> deviceUuids = new ArrayList<>();
        deviceUuids.add("51YMO52WXW07");
        deviceUuids.add("4ZP4NAOW15C7");
        requestBody.put("deviceUuids", deviceUuids);
        String result = RequestUtil.postRequest("/dev-api/api/v1/queryDeviceDetail", requestBody);
        System.out.println("queryDeviceDetail result：" + result);
    }

    /**
     * 获取设备的实时数据
     *
     * 无数据
     */
    @Test
    public void getDeviceRealTimeData() {
        HashMap<String, Object> requestBody = new HashMap<>();
        List<String> deviceUuids = new ArrayList<>();
        deviceUuids.add("51YMO52WXW07");
        requestBody.put("deviceUuid", deviceUuids);

        List<String> properties = new ArrayList<>();
        properties.add("SR01");
        requestBody.put("properties", properties);

        List<String> thingModel = new ArrayList<>();
        thingModel.add("7E6rc0l~sO8pgeWrwzgHaf");
        requestBody.put("thingModel", thingModel);

        String result = RequestUtil.postRequest("/dev-api/api/v1/getDeviceRealTimeData", requestBody);
        System.out.println("getDeviceRealTimeData result：" + result);
    }


    /**
     * 查询设备数据
     *
     * 成功
     */
    @Test
    public void queryDeviceData() {
        HashMap<String, Object> requestBody = new HashMap<>();
//        requestBody.put("deviceCode", "51YMO52WXW07");
//        requestBody.put("deviceName", "拉土机测试");
        requestBody.put("page", 1);
        requestBody.put("size",60);
        String result = RequestUtil.postRequest("/dev-api/api/v1/queryDeviceData", requestBody);
        System.out.println("queryDeviceData result：" + result);
    }

    /**
     * 设备注册
     */
    @Test
    public void deviceRegisterTest() {
        SignClient signClient = new DefaultSignClient("http://developer-api-gateway.bdn-yunbu-dev.rootcloudapp.com",
                "WtkI2", "tK-q-LybSAOh-cBTBjm8Owh0zbLF4MTFOBxtAuuWowfQ");
        CommonRequest request = new CommonRequest();
        String tenantId = "";
        request.setApiUri("/base/base-open-api/api/openapi/v1/device/register/bunch/" + tenantId + "/new"); // 请求Api地址
        request.setNeedToken(false);// 是否需要token
        request.setContentType(ContentType.JSON);// 请求头类型
        request.setMethod(HttpMethod.POST);// 请求方法

        HashMap<String, Object> requestBody = new HashMap<>();
        List<Map<String, Object>> bDeviceRegisterDTOList = new ArrayList<>();
        Map<String, Object> body = new HashMap<>();
        body.put("deviceName", "test0001");
        body.put("deviceUuid", "");
        body.put("servicePackageId", "");
        body.put("sn", "");
        body.put("tenantId", "");
        body.put("thingUuid", "");
        body.put("thingVersion", "");

        bDeviceRegisterDTOList.add(body);
        requestBody.put("bDeviceRegisterDTOList", bDeviceRegisterDTOList);
        request.setRequestBody(requestBody);
        String result = signClient.execute(request);
        System.out.println("deviceRegisterTest result：" + result);
    }

    /**
     * 设备变更
     */
    @Test
    public void deviceChangeTest() {
        SignClient signClient = new DefaultSignClient("http://developer-api-gateway.bdn-yunbu-dev.rootcloudapp.com",
                "WtkI2", "tK-q-LybSAOh-cBTBjm8Owh0zbLF4MTFOBxtAuuWowfQ");
        CommonRequest request = new CommonRequest();
        String tenantId = "";
        request.setApiUri("/base/base-open-api/api/openapi/v1/device/register/bunch/" + tenantId + "/new"); // 请求Api地址
        request.setNeedToken(false);// 是否需要token
        request.setContentType(ContentType.JSON);// 请求头类型
        request.setMethod(HttpMethod.PUT);// 请求方法

        HashMap<String, Object> requestBody = new HashMap<>();
        List<Map<String, Object>> deviceChengeDTOList = new ArrayList<>();
        Map<String, Object> body = new HashMap<>();
        body.put("deviceName", "");
        body.put("deviceUuid", "");
        deviceChengeDTOList.add(body);
        requestBody.put("deviceChengeDTOList", deviceChengeDTOList);
        request.setRequestBody(requestBody);
        String result = signClient.execute(request);
        System.out.println("deviceChangeTest result：" + result);
    }

    private static String lastDate() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, -1);
        return formatDate(c.getTime());
    }

    private static String nowDate() {
        return formatDate(new Date());
    }

    private static String formatDate(Date date) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.CHINA);
        return sdf.format(date);
    }
}
