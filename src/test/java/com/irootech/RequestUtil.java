package com.irootech;

import com.rootcloud.spruce.api.sdk.client.SignClient;
import com.rootcloud.spruce.api.sdk.client.support.DefaultSignClient;
import com.rootcloud.spruce.api.sdk.common.base.CommonRequest;
import com.rootcloud.spruce.api.sdk.common.enums.ContentType;
import com.rootcloud.spruce.api.sdk.common.enums.HttpMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RequestUtil {

//    private static final String ROOT_URI = "http://developer-api-gateway.bdn-yunbu-dev.rootcloudapp.com";
//    private static final String APP_ID = "2uVY0";
//    private static final String APP_SECRET = "aVHs5jmNRdGGHKf-1j2GTQLLTgUBzFQJe1A4qJIzoO4Q";
//    private static final String ROOT_URI = "http://dev-api.bdn-yunbu-dev.rootcloudapp.com";

//    private static final String APP_ID = "5Mvp1";
//    private static final String APP_SECRET = "kBmk1wl8RtuqKcJZBmKntwUeclUf8PRJGUPYal6n0XLw";
    //卫华生产
//    private static final String ROOT_URI = "https://apidev.cranewh.com";
//    private static final String APP_ID = "NDrO0";
//    private static final String APP_SECRET = "vQnqE_N-SG2wLG3zVw7liguwqIWTfVQS2gxcwG18X4TQ";

    //专属云qa
    private static final String ROOT_URI = "http://developer-api-gateway.bdn-yunbu-qa.rootcloudapp.com";
    private static final String APP_ID = "6Xka0";
    private static final String APP_SECRET = "xpAmtiF6Sxed3L9TReNcPgxK2PlagcSDmE6HfcQ5FC2w";

    public static String postRequest(String apiUrl, HashMap<String, Object> requestBody) {
        SignClient signClient = new DefaultSignClient(ROOT_URI, APP_ID, APP_SECRET);
        CommonRequest request = new CommonRequest();
        request.setApiUri(apiUrl); // 请求Api地址
        request.setNeedToken(false);// 是否需要token
        request.setContentType(ContentType.JSON);// 请求头类型
        request.setMethod(HttpMethod.POST);// 请求方法
        request.setRequestBody(requestBody);// 请求体x
        return signClient.execute(request);
    }

    public static String postRequestForHeader(String apiUrl, Map<String, String> headers, HashMap<String, Object> requestBody) {
        SignClient signClient = new DefaultSignClient(ROOT_URI, APP_ID, APP_SECRET);
        CommonRequest request = new CommonRequest();
        request.setApiUri(apiUrl); // 请求Api地址
        request.setNeedToken(false);// 是否需要token
        request.setContentType(ContentType.JSON);// 请求头类型
        request.setMethod(HttpMethod.POST);// 请求方法
        request.setRequestBody(requestBody);// 请求体x
        request.setHeaders(headers);// 请求体x
        return signClient.execute(request);
    }

    public static String postRequestForArray(String apiUrl, ArrayList<Map<String, Object>> requestBody) {
        SignClient signClient = new DefaultSignClient(ROOT_URI, APP_ID, APP_SECRET);
        CommonRequest request = new CommonRequest();
        request.setApiUri(apiUrl); // 请求Api地址
        request.setNeedToken(false);// 是否需要token
        request.setContentType(ContentType.JSON);// 请求头类型
        request.setMethod(HttpMethod.POST);// 请求方法
        request.setRequestBody(requestBody);// 请求体x
        return signClient.execute(request);
    }

    public static String getRequest(String apiUrl) {
        SignClient signClient = new DefaultSignClient(ROOT_URI, APP_ID, APP_SECRET);
        CommonRequest request = new CommonRequest();
        request.setApiUri(apiUrl); // 请求Api地址
        request.setNeedToken(false);// 是否需要token
        request.setContentType(ContentType.FORM_URLENCODED);// 请求头类型
        request.setMethod(HttpMethod.GET);// 请求方法
        return signClient.execute(request);
    }

    public static String getRequestForParams(String apiUrl, Map<String, String> params) {
        SignClient signClient = new DefaultSignClient(ROOT_URI, APP_ID, APP_SECRET);
        CommonRequest request = new CommonRequest();
        request.setApiUri(apiUrl); // 请求Api地址
        request.setNeedToken(false);// 是否需要token
        request.setContentType(ContentType.FORM_URLENCODED);// 请求头类型
        request.setMethod(HttpMethod.GET);// 请求方法
        request.setParams(params);// 请求体x
        return signClient.execute(request);
    }

    public static String getRequestForHeaderAndParams(String apiUrl, Map<String, String> headers, Map<String, String> params) {
        SignClient signClient = new DefaultSignClient(ROOT_URI, APP_ID, APP_SECRET);
        CommonRequest request = new CommonRequest();
        request.setApiUri(apiUrl); // 请求Api地址
        request.setNeedToken(false);// 是否需要token
        request.setContentType(ContentType.FORM_URLENCODED);// 请求头类型
        request.setMethod(HttpMethod.GET);// 请求方法
        request.setParams(params);// 请求体x
        request.setHeaders(headers);// 请求体x
        return signClient.execute(request);
    }
}
