package com.irootech;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
public class TenantApiTest {

    /**
     * 获取租户信息接口
     * <p>
     * 成功
     */
    @Test
    public void getTenantDetailTest() {
        HashMap<String, Object> requestBody = new HashMap<>();
        String result = RequestUtil.postRequest("/dev-api/api/v1/getTenantDetail", requestBody);
        System.out.println("getTenantDetail result：" + result);
    }
}
