package com.irootech;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class WeihuaTest {

    @Test
    public void queryphoneValiCode() {
        Map<String,String> params=new HashMap<>();
        params.put("telphone","15330087501");

        String rs=RequestUtil.getRequestForParams("/base/base-identity/api/user/verification-code-for-login",params);
        System.out.println(rs);
    }


    @Test
    public void phoneLogin() {

        /**
         * {
         *   "phoneNumber": "17622221111",
         *   "verificationCode": "265404"
         * }
         */
        HashMap<String, Object> requestBody = new HashMap<>();
        requestBody.put("phoneNumber","15330087501");
        requestBody.put("verificationCode","894995");

        String result = RequestUtil.postRequest("/iot/auth/login-by-verification-code", requestBody);
        System.out.println("queryAlarmData result：" + result);

    }
}
