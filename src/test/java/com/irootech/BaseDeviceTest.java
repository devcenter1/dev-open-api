package com.irootech;

import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class BaseDeviceTest {

    @Test
    public void queryDeviceListTest() {
        String loginResult = loginTest();
        JSONObject loginJson = JSONObject.parseObject(loginResult);
        HashMap<String, Object> requestBody = new HashMap<>();
        List<String> authorityOfMeTenantIds = new ArrayList<>();
        requestBody.put("authorityOfMeTenantIds", authorityOfMeTenantIds);
        requestBody.put("authorityToMeTenantId", "");
        requestBody.put("checkAlarm", false);
        requestBody.put("deviceName", "");
        requestBody.put("deviceCode", "");
        requestBody.put("showFocus", false);
        requestBody.put("page", "1");
        requestBody.put("size", "10");

        Map<String, String> headers = new HashMap<>();
        headers.put("current_app", "A0003");
        headers.put("current_tenant", "ARIME07");
        headers.put("Authorization", "Bearer " + loginJson.get("access_token"));
        System.out.println(JSONObject.toJSON(headers));
        String result = RequestUtil.postRequestForHeader("/saas-dev/saas-aba-base/api/V1/device-infos/device-list-saas", headers, requestBody);
        System.out.println("queryDeviceListTest result：" + result);
    }

    @Test
    public void loginApiTest() {
        String result = loginTest();
        System.out.println("queryDeviceListTest result：" + result);
    }

    public String loginTest() {
        HashMap<String, Object> requestBody = new HashMap<>();
        requestBody.put("password", "zH+O/aAIUje74tjEDgA8zw==");
        requestBody.put("username", "6MQ+BmAR+oPJQVcasFiMJQ==");
        return RequestUtil.postRequest("/saas-dev/auth/login", requestBody);
    }
}
